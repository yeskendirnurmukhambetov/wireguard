# Wireguard install and configuration on Linux

**Prerequisites:**
- Linux Server running Ubuntu 20.04 LTS or newer

**Installation and Configuration**

**On server:**

- **Install wireguard:**
```
sudo apt update
sudo apt install wireguard
```

- **Generate keys:**
```
wg genkey | sudo tee /etc/wireguard/private.key
sudo chmod go= /etc/wireguard/private.key
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
```


- **Creating a WireGuard Server Configuration:**
```
sudo vim /etc/wireguard/wg0.conf
[Interface]
PrivateKey = <server's private key>
Address = 10.8.0.1/24
ListenPort = 51820
SaveConfig = true
PostUp = ufw route allow in on wg0 out on eth0
PostUp = iptables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
PostUp = ip6tables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
PreDown = ufw route delete allow in on wg0 out on eth0
PreDown = iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
PreDown = ip6tables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

```

- **Adjusting the WireGuard Server’s Network Configuration:** 

`sudo vim /etc/sysctl.conf`
```
/etc/sysctl.conf
net.ipv4.ip_forward=1
```

- **To read the file and load the new values for your current terminal session, run:**
```
sudo sysctl -p
```


- **Configuring the WireGuard Server’s Firewall:**
```
sudo ufw allow 51820/udp
sudo ufw allow OpenSSH
```


- **After adding those rules, disable and re-enable UFW to restart it and load the changes from all of the files you’ve modified:**


```
sudo ufw disable
sudo ufw enable
```



- **Starting the WireGuard Server:**
```
sudo systemctl enable --now wg-quick@wg0.service
```

**On client:**


- **Generate keys:**
```
wg genkey | sudo tee /etc/wireguard/private.key
sudo chmod go= /etc/wireguard/private.key
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
```


- **Configuring a WireGuard Peer:**
```
[Interface]
PrivateKey = <client's private key>
Address = 10.8.0.x/24

[Peer]
PublicKey = <server's public key>
AllowedIPs = 0.0.0.0/0
Endpoint = <server's ip>:51820
```
**On server:**


- **Adding the Peer’s Public Key to the WireGuard Server at serve:r**
```
sudo wg set wg0 peer <client's public key> allowed-ips 10.8.0.x/24
```
- **To add new client repeat previous step with another ip**

- **Save config:**
```
wg-quick save wg0
```

